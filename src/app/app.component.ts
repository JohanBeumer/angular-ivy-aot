import { Component, OnInit } from '@angular/core';
import { Foo } from './model/foo.model';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
    title = 'shiney-ivy-app';
    classname = '';

    ngOnInit() {
        this.classname = Foo.classsname;
    }
}
