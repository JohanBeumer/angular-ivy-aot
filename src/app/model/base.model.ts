export class BaseModel {
    id = -1;
    // classname = this.constructor.toString().match(/\w+/g)[1];
    loadData(plainObject) {

        for (const key in plainObject) {
            if (plainObject.hasOwnProperty(key)) {
                this[key] = plainObject[key];
            }
        }
    }
}
